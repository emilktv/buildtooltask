package it;

import initializers.SeleniumInitializer;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class BookFightIT {
    WebDriver driver;
    @Before
    public void setup(){
        String browser=System.getProperty("browser");
        if(browser.toLowerCase().equals("chrome")){
            WebDriverManager.chromedriver().setup();
            ChromeOptions options=new ChromeOptions();
            options.setHeadless(true);
            driver=new ChromeDriver(options);
        }
        else if(browser.toLowerCase().equals("firefox")){
            WebDriverManager.firefoxdriver().setup();
            FirefoxOptions options = new FirefoxOptions();
            options.setHeadless(true);
            driver=new FirefoxDriver(options);
        }
        driver.get(System.getProperty("url"));
    }
    @Test
    public void bookFlightIT(){
        String email="a@g.in";String password="1234";String confirmPassword="1234";String passengerCount="2";String toPort="London";String dateDay="23";String airlineIndexNumber="1";
        SeleniumInitializer seleniumInitializer=new SeleniumInitializer();
        seleniumInitializer.registerProcess(driver,email,password,password);
        seleniumInitializer.bookFlightProcess(driver,email,password,passengerCount,toPort,dateDay,airlineIndexNumber);
        String result=driver.getTitle();
        Assert.assertEquals(result,"Find a Flight: Mercury Tours:");
    }
    @After
    public void teardown(){
        driver.close();
    }
}
